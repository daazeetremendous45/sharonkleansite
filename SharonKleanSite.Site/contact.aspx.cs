﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Net;
namespace SharonKleanSite.Site
{
    public partial class contact : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                success.Visible = false;
                error.Visible = false;
            }
        }

        protected void submit_Click(object sender, EventArgs e)
        {
            try
            {
                MailMessage mail = new MailMessage();

                //set the addresses 
                mail.From = new MailAddress("test@sharonklean.com"); //IMPORTANT: This must be same as your smtp authentication address.
                mail.To.Add("azeez.adedayo@yahoo.com");

                //set the content 
                mail.Subject = $"Info From : {Request.Form["name"]}";
                mail.Body = "<div>" +
                            $"<div style='font-size:20px;'><b>{Request.Form["name"]}</b></div>" +
                            $"<div> {Request.Form["phone"]} </div>" +
                            $"<div> {Request.Form["email"]}  </div>" +
                            $"<div style='width:500px; text-align:justify'><p> {Request.Form["message"]}</p></div>" +
                            "</div>";
                //send the message 
                SmtpClient smtp = new SmtpClient("mail.sharonklean.com");
                mail.IsBodyHtml = true;
                //IMPORANT:  Your smtp login email MUST be same as your FROM address. 
                NetworkCredential Credentials = new NetworkCredential("test@sharonklean.com", "yakdseeker@1");
                smtp.Credentials = Credentials;
                smtp.Port = 25;
                smtp.Send(mail);
                Request.Form["name"] = "";
                Request.Form["phone"] = "";
                Request.Form["email"] = "";
                Request.Form["message"] = "";
                success.Text = "Your message was sent successfully, We will be in touch as soon";
                success.Visible = true;
            }

            catch (Exception ex)
            {
                string message = ex.Message.ToString();
                error.Text = "Something went wrong, try refreshing and submitting the form again.";
                error.Visible = true;
            }
            // lblMessage.Text = "Mail Sent";
        }
    }
}